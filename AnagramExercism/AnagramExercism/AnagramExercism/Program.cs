﻿namespace AnagramExercism
{
    public class Program
    {
        static void Main(string[] args)
        {
            string[] candidates = new[] { "enlists", "google", "inlets", "banana" };
            Anagram baseWord = new Anagram("listen");

            string[] result = baseWord.FindAnagrams(candidates);

            foreach(string res in result){
                Console.WriteLine(res);
            }
        }
    }
}