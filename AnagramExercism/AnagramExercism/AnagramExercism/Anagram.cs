﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnagramExercism
{
    public class Anagram
    {
        public string Candidate { get; set; }

        public Anagram(string baseWord)
        {
            Candidate = baseWord.ToLower();
        }

        public string[] FindAnagrams(string[] potentialMatches)
        {
            List<string> result = new List<string>();
            foreach(var anagram in potentialMatches)
            {
                if(anagram != null && anagram.Length == Candidate.Length && anagram.ToLower() != Candidate)
                {
                    var chars1 = anagram.ToLower().ToCharArray().OrderBy(x => x);
                    var chars2 = Candidate.ToCharArray().OrderBy(x => x);

                    if (chars1.SequenceEqual(chars2) is true)
                        result.Add(anagram);
                }
            }
            return result.ToArray();
        }
    }
}
